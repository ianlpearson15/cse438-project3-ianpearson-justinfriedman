package com.example.cse438.blackjack

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AlertDialog
import android.util.Log

import android.widget.ImageView

import android.widget.Toast

import com.example.cse438.blackjack.util.CardRandomizer
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import android.R.attr.y
import android.R.attr.x
import android.graphics.Point

import android.view.View

import android.text.TextUtils
import android.view.*



lateinit var mAuth: FirebaseAuth
lateinit var currentUser: FirebaseUser

lateinit var db: FirebaseFirestore
var userDocumentId: String = ""
var dealerCardCount = 0
var playerCardCount = 0


class MainActivity : AppCompatActivity() {
    var dealerBackCardId: Int = 0
    var dealerBackCardTargetId: Int = 0
    var imageViews = arrayListOf<Int>()
    var idIncriment: Int = 0
    private lateinit var myDetector: GestureDetectorCompat

    private val deck: ArrayList<Card> = ArrayList()

    private var game : Game? = null

    private var nextWager = 0

    private var firstRun = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        myDetector = GestureDetectorCompat(this, MyGestureListener())

        mAuth = FirebaseAuth.getInstance()
        currentUser = mAuth.currentUser!!
        db = FirebaseFirestore.getInstance()

        Log.d("Current User", currentUser.email.toString())

        Log.d("DEBUG", "this is a test of the emergency broadcast system")

        leader_board_button.setOnClickListener {
            val intent = Intent(this, LeaderBoard::class.java)
            startActivity(intent)


        }

        //create deck of cards
        val randomizer = CardRandomizer()
        var idList: ArrayList<Int> = randomizer.getIDs(this) as ArrayList<Int>
        for(id in idList) {
            val name = resources.getResourceEntryName(id)
            val card = Card(id, name)
            deck.add(card)
        }


        logout_button.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Logout")
            builder.setMessage("Do you really want to log out?")
            var dialog: AlertDialog
            builder.setPositiveButton("No") {dialogInterface, i ->


            }
            builder.setNegativeButton("Yes") { dialogInterface, i ->
                mAuth.signOut()
                //val intent = Intent(this, Register::class.java)
                //startActivity(intent)
                finish()
            }

            dialog = builder.create()
            dialog.show()
        }

        double_down_button.setOnClickListener {
            doubleDown()
        }

        betting_checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            betBoxToggled(isChecked)
        }

        coin_bet.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                setWager()
                return@OnKeyListener true
            }
            false
        })

        winLossLabelUpdate()

        newGame()
        
    }


    override fun onTouchEvent(event: MotionEvent?): Boolean {
        Log.d("GESTURE", "touch event detected")
        myDetector.onTouchEvent(event)
        return super.onTouchEvent(event)
    }

    fun playerHitGesture() {

        Log.d("ACTION", "Hit gesture!")
        val canContinue = game?.playerHit()
        val dealt = game!!.getLastDealtCard()
        addCardAnimPlayer(dealt)
        if(canContinue == false) {
            gameOver(canContinue)
        }
    }

    fun playerStandGesture() {
        Log.d("ACTION", "Stand gesture!")

        while(game!!.dealerDeal()) {
            addCardAnimDealer(game!!.getLastDealtCard())
        }
        val playerWin = game!!.playerStand()
        gameOver(playerWin)
    }

    fun doubleDown() {
        Log.d("ACTION", "Double down button hit!")
        var currWager = game!!.getWager()
        val currPurse = coins_val.text.toString().toInt()
        if(currWager > coins_val.text.toString().toInt()) {
            val message = "Unable to double down! Requires " + currWager + " coins, you have " + currPurse + "."
            Toast.makeText(this@MainActivity,
                message,
                Toast.LENGTH_SHORT).show()
        } else {
            gameOver(game!!.doubleDown())
        }
    }

    private fun gameOver(playerWin: Boolean) {

        var message = ""
        if(playerWin) {
            message = "Player won!"
        }
        else {
            message = "player lost!"
        }
        Log.d("GAMEOVER", message)
        Log.d("GAMEOVER", "Starting new game!")

        val wager = game!!.getWager()

        winLossUpdate(playerWin, wager)

        newGame()

    }

    private fun setWager() {
        Log.d("WAGER", "Wager being set!")
        var wager = coin_bet.text.toString().toInt()

        Log.d("WAGER","Wager valid")
        val message = "Wager set to " + wager + " coins."
        Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
        Log.d("WAGER", "wager set to " + wager)

        nextWager = wager
        next_wager_val.text = "" + wager
    }


    private fun newGame() {
        dealerCardCount = 0
        playerCardCount = 0
        goneImageViews()
        imageViews.clear()


        if(betting_checkbox.isChecked()) {
            game = Game(deck, nextWager)
        } else {
            game = Game(deck)
        }

        wager_val.text = "" + nextWager

        val playerHand = game!!.getPlayerHand()
        val dealerHand = game!!.getDealerHand()

        addCardAnimPlayer(playerHand.get(0))
        Log.d("ANIMATION", "Adding player card: " + playerHand.get(0).name)
        addCardAnimPlayer(playerHand.get(1))
        Log.d("ANIMATION", "Adding player card: " + playerHand.get(1).name)
        addCardAnimDealer(dealerHand.get(0))
        Log.d("ANIMATION", "Adding dealer card: " + dealerHand.get(0).name)
        addCardAnimDealer(dealerHand.get(1))
        Log.d("ANIMATION", "Adding dealer card: " + dealerHand.get(1).name)


        if(game!!.checkDealerBlackjack()) {
            Log.d("NEWGAME", "Dealer natural blackjack! GG")
            gameOver(game!!.DealerWinCondition)
        } else if(game!!.checkPlayerBlackjack()) {
            Log.d("NEWGAME", "Player natural blackjack for the win! GG")
            gameOver(game!!.PlayerWinCondition)
        } else {
            Log.d("NEWGAME", "Regular old new game")
        }

    }

    private fun betBoxToggled(isChecked: Boolean) {
        if(game!!.isGameOver()) {
            if(!isChecked) {
                coin_bet.visibility = View.INVISIBLE
                newGame()
            } //should only go from checked to unchecked, since we're only stuck in game over state if betting is on but no bet has been entered
        } else {
            if(isChecked) {
                coin_bet.visibility = View.VISIBLE
                wager_label.visibility = View.VISIBLE
                wager_val.visibility = View.VISIBLE
                next_wager_label.visibility = View.VISIBLE
                next_wager_val.visibility = View.VISIBLE
                val message = "Betting enabled for next round! Enter your wager and hit enter to activate it."
                Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
            } else {
                coin_bet.visibility = View.INVISIBLE
                wager_label.visibility = View.INVISIBLE
                wager_val.visibility = View.INVISIBLE
                next_wager_label.visibility = View.INVISIBLE
                next_wager_val.visibility = View.INVISIBLE
                val message = "Betting disabled for next round! Your bet is still locked in for this round."
                Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun winLossLabelUpdate() {
        db.collection("users").whereEqualTo("email", currentUser.email)
            .get()
            .addOnSuccessListener { documents ->
                lateinit var user: User
                for (document in documents) {
                    userDocumentId = document.id
                    user = document.toObject(User::class.java)
                }
                wins_val.text = user.wins.toString()
                losses_val.text = user.losses.toString()
                coins_val.text = user.coins.toString()
            }.addOnFailureListener { exception ->
                Log.w("winLossLabelUpdate", "Error getting documents: ", exception)
            }
    }

    private fun winLossUpdate(won: Boolean, wager: Int = 0) {
        lateinit var user: User

        val userRef = db.collection("users").document(userDocumentId)

        userRef.get().addOnSuccessListener { document ->

            user = document.toObject(User::class.java)!!
            if (won) {
                userRef
                    .update("wins", user.wins+1)
                    .addOnSuccessListener { Log.d("win update", "DocumentSnapshot successfully updated!") }
                    .addOnFailureListener { e -> Log.w("win update", "Error updating document", e) }
            }
            else {
                userRef
                    .update("losses", user.losses+1)
                    .addOnSuccessListener { Log.d("loss update", "DocumentSnapshot successfully updated!") }
                    .addOnFailureListener { e -> Log.w("loss update", "Error updating document", e) }
            }
            var coinChange = wager
            if (coinChange != 0) {
                if(!won) {
                    coinChange = 0 - wager
                }
                userRef
                    .update("coins", user.coins+coinChange)
                    .addOnSuccessListener { Log.d("coin update", "DocumentSnapshot successfully updated!") }
                    .addOnFailureListener { e -> Log.w("coin update", "Error updating document", e) }
            }

            winLossLabelUpdate()

        }

    }

    private fun addCardAnimPlayer(card: Card) {
        val id = card.id
        val constraintLayout = findViewById(R.id.cl) as ConstraintLayout
        val imageView = ImageView(this)
        imageView.setImageResource(id)
        imageView.id = idIncriment
        idIncriment += 1
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val maxX = size.x
        val maxY = size.y



        constraintLayout.addView(imageView)

        val animSetXY = AnimatorSet()


        imageViews.add(imageView.id)

        val x = ObjectAnimator.ofFloat(
            imageView,
            "translationX",
            imageView.translationX,
            (30 + 100* playerCardCount).toFloat()
        )

        val y = ObjectAnimator.ofFloat(
            imageView,
            "translationY",
            imageView.translationY,
            820.toFloat()
        )

        animSetXY.playTogether(x, y)
        animSetXY.duration = 300
        animSetXY.start()
        playerCardCount += 1



    }

    private fun addCardAnimDealer(card: Card) {
        val id = card.id
        val constraintLayout = findViewById(R.id.cl) as ConstraintLayout
        val imageView = ImageView(this)
        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val maxX = size.x
        val maxY = size.y
        imageView.id = idIncriment
        idIncriment += 1
        if (dealerCardCount == 0) {
            imageView.setImageResource(R.drawable.back)

        }
        else {
            imageView.setImageResource(id)
        }

        imageViews.add(imageView.id)



        if(dealerCardCount == 2) {
            flipDealerCard()
        }





        constraintLayout.addView(imageView)

        val animSetXY = AnimatorSet()

        val x = ObjectAnimator.ofFloat(
            imageView,
            "translationX",
            imageView.translationX,
            (30 + 100* dealerCardCount).toFloat()
        )

        val y = ObjectAnimator.ofFloat(
            imageView,
            "translationY",
            imageView.translationY,
            170.toFloat()
        )

        animSetXY.playTogether(x, y)
        animSetXY.duration = 300
        animSetXY.start()

        if (dealerCardCount == 0) {
            dealerBackCardId = imageView.id
            dealerBackCardTargetId = id

        }
        dealerCardCount += 1



    }

    private fun flipDealerCard() {
        val imageView = findViewById<ImageView>(dealerBackCardId)
        imageView.setImageResource(dealerBackCardTargetId)

    }

    private fun goneImageViews() {
        for (each in imageViews) {
            val imageView = findViewById<ImageView>(each)
            Log.d("goneImageViews", each.toString())
            imageView.visibility = View.GONE
        }
    }

    private inner class MyGestureListener : GestureDetector.SimpleOnGestureListener() {

        private var swipedistance = 150

        override fun onDoubleTap(e: MotionEvent?): Boolean {
            Log.d("GESTURE", "Double tap")
            playerStandGesture()
            return true
        }

        override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {

            if(e2.x - e1.x > swipedistance) {
                Log.d("GESTURE", "Right swipe")
                playerHitGesture()
            }
            return true
        }

    }

}


