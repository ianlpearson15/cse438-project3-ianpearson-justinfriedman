package com.example.cse438.blackjack

import android.util.Log
import java.util.*


class Game(theDeck: ArrayList<Card>, theWager: Int = 0) {

    //placeholders so we don't just return true or false down below
    val PlayerWinCondition = true
    val DealerWinCondition = false

    val PlayerHitSuccessful = true
    val PlayerHitBust = false

    private val deck: ArrayList<Card> = ArrayList()

    private var playerHand: ArrayList<Card> = ArrayList()
    private var dealerHand: ArrayList<Card> = ArrayList()

    private var wager = theWager

    private var doubledDown = false

    val rand: Random = Random()

    private var gameOver = false

    //keep track of the last dealt card so we can animate it in MainActivity
    private var lastDealtCard: Card = Card(0, "")

    init {
        deck.addAll(theDeck)
        //deal initial two cards
        playerHand = dealCard(playerHand)
        playerHand = dealCard(playerHand)

        dealerHand = dealCard(dealerHand)
        dealerHand = dealCard(dealerHand)

    }

    fun checkPlayerBlackjack() : Boolean {
        return (getTotalValue(playerHand) == 21)
    }

    fun checkDealerBlackjack() : Boolean {
        return (getTotalValue(dealerHand) == 21)
    }

    //return true for hit successful, return false for bust
    fun playerHit() : Boolean {
        playerHand = dealCard(playerHand)
        if(getTotalValue(playerHand) > 21) {
            Log.d("GAMEHIT", "Player bust! Game over! Player hand score: " + getTotalValue(playerHand))
            gameOver = true
            return PlayerHitBust
        }
        Log.d("GAMEHIT", "Player hit successful! Player hand score: " + getTotalValue(playerHand))
        return PlayerHitSuccessful

    }

    //return true for player win, false for dealer win
    fun playerStand() : Boolean {
        gameOver = true
        val playerVal = getTotalValue(playerHand)
        var dealerVal = getTotalValue(dealerHand)

        /*
        while(dealerVal <= 17) {
            Log.d("GAMESTAND", "Dealer hand under 17, so he hits. Current hand: " + dealerVal)
            dealerHand = dealCard(dealerHand)
            dealerVal = getTotalValue(dealerHand)
            Log.d("GAMESTAND", "New dealer hand: " + dealerVal)
        }
        */

        if(dealerVal > 21 || playerVal > dealerVal) {
            Log.d("GAMESTAND", "Player hand beats dealer hand! Player hand: " + playerVal + " Dealer hand: " + dealerVal)
            return PlayerWinCondition
        }
        Log.d("GAMESTAND", "Player hand loses! Player hand: " + playerVal + " Dealer hand: " + dealerVal)
        return DealerWinCondition
    }

    fun dealerDeal() : Boolean {
        var dealerVal = getTotalValue(dealerHand)
        if(dealerVal <= 17) {
            dealerHand = dealCard(dealerHand)
            return true
        }
        return false
    }

    fun getDealerHandValue() : Int {
        return getTotalValue(dealerHand)
    }

    //doubles wager, hits, then stands (assuming player didn't bust)
    fun doubleDown() : Boolean {
        /*
        if(doubledDown) {
            return false //if we've already doubled down, don't let them do it again
        }
        */
        wager *= 2
        if(playerHit()) {
            return playerStand() //player doesn't bust, so they stand
        } else {
            return DealerWinCondition //player busted
        }

    }

    fun getPlayerHand() : ArrayList<Card> {
        return playerHand
    }

    fun getDealerHand() : ArrayList<Card> {
        return dealerHand
    }

    fun getWager() : Int {
        return wager
    }

    fun getLastDealtCard() : Card {
        return lastDealtCard
    }

    fun isGameOver() : Boolean {
        return gameOver
    }


    private fun dealCard(hand: ArrayList<Card>): ArrayList<Card> {
        if(deck.size == 0) {
            return hand
        }
        var index = rand.nextInt(deck.size)
        val dealt = deck.get(index)
        hand.add(dealt)
        Log.d("GAMEDEAL", "Card dealt: " + deck.get(index).name)
        lastDealtCard = dealt
        deck.removeAt(index)
        return hand
    }

    private fun getTotalValue(hand: ArrayList<Card>) : Int {
        var total = 0
        for(card in hand) {
            total += getCardValue(card.name)
        }
        //account for the fact that we treat aces as 1 in getCardValue
        if(handContainsAce(hand)) {
            if((total+10) <= 21 ) {
                total += 10
            }
        }
        return total
    }

    private fun handContainsAce(hand: ArrayList<Card>) : Boolean {
        for(card in hand) {
            if(card.name.contains("ace")) {
                Log.d("GAMEACECHECK", "Hand contains an ace!")
                return true
            }
        }
        return false
    }

}
