package com.example.cse438.blackjack

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class LeaderBoardAdapter(val context: Context, val source: ArrayList<LeaderBoardRow>) : BaseAdapter() {

    val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun getCount(): Int {
        return source.size
    }

    override fun getItem(p0: Int): Any {
        return source[p0]
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {

        val rowView = inflater.inflate(R.layout.leader_board_row,p2, false)

        val userNameTextView = rowView.findViewById(R.id.user_name) as TextView
        val winLossTextView = rowView.findViewById(R.id.win_loss) as TextView
        val coinsTextView = rowView.findViewById(R.id.coins) as TextView

        val row = getItem(p0) as LeaderBoardRow

        userNameTextView.text = row.username
        var winPercent = (row.win2loss*100).toInt().toString() + "%"
        winLossTextView.text = winPercent
        coinsTextView.text = row.coins.toString()

        return rowView

    }
}