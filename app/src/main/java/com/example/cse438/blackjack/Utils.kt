package com.example.cse438.blackjack



fun getCardValue(name: String) : Int {
    var value: Int = 0

    if(name.contains("ace")) {
        return 1
    }
    else if(name.contains("jack") || name.contains("queen") || name.contains("king")) {
        return 10
    }
    else {
        return name.replace("\\D".toRegex(), "").toInt()
    }
}