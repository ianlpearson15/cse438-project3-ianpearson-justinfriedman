package com.example.cse438.blackjack

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.ListView
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_leader_board.*

class LeaderBoard : AppCompatActivity() {
    //on start: query for all data, display sorted list based off the ratio selected
    //on toggle: display sorted list based off the ratio selected

    lateinit var db: FirebaseFirestore
    var rows = arrayListOf<LeaderBoardRow>()
    private lateinit var listView : ListView
    lateinit var adapter : LeaderBoardAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leader_board)
        db = FirebaseFirestore.getInstance()

        //set up adapter on list view
        this.listView = findViewById(R.id.leader_board_list)
        adapter = LeaderBoardAdapter(this, rows)
        listView.adapter = adapter

        leader_board_toggle.setOnCheckedChangeListener { buttonView, isChecked ->
            sortRows()
        }
    }


    override fun onResume() {
        super.onResume()
        queryAndConvert()
    }



    fun queryAndConvert() {
        db.collection("users").get().addOnSuccessListener { result ->

            rows.clear()
            for (document in result) {

                val user = document.toObject(User::class.java)
                val row : LeaderBoardRow = LeaderBoardRow()
                row.username = user.username

                if (user.losses == 0 && user.wins == 0) {
                    row.win2loss = 0.toFloat()
                }
                else {
                    row.win2loss = user.wins.toFloat() / (user.losses.toFloat() + user.wins.toFloat())
                }
                Log.d("win loss", user.wins.toString() + " " + user.losses.toString())
                Log.d("win2loss", row.win2loss.toString())

                row.coins = user.coins

                rows.add(row)

                sortRows()




            }
        }.addOnFailureListener { exception ->
            Log.d("QueryAndConvert", "Error getting documents", exception)
        }
    }

    fun sortRows() {
        Log.d("Leader Board", "sortRows")
            if (leader_board_toggle.isChecked) {
                var sorted = rows.sortedWith(compareByDescending({it.coins}))
                rows.clear()
                rows.addAll(sorted)

                adapter.notifyDataSetChanged()

            } else {
                var sorted = rows.sortedWith(compareByDescending({it.win2loss}))
                rows.clear()
                rows.addAll(sorted)

                adapter.notifyDataSetChanged()
            }

    }
}
