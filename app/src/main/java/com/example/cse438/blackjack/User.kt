package com.example.cse438.blackjack

class User {

    var email: String = ""
    var username: String = ""
    var wins: Int = 0
    var losses: Int = 0
    var coins: Int = 50
}