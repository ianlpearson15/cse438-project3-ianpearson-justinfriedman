In this file you should include:

Any information you think we should know about your submission
* Is there anything that doesn't work? Why?
If the player or dealer is dealt a natural blackjack on start, it crashes the app.
it seems to execute before certain elements relating to the firebase database are created on time,
even though newGame runs after all of that stuff should be created - we are not totally sure why.

Also, we have a bit of an issue in our FirebaseFirestore static field variable, in that
it shouldn't be a static field variable - we weren't able to find a straightforward solution to 
this and it wasn't causing us excessive problems so we left it.

* Is there anything that you did that you feel might be unclear? Explain it here.

A description of the creative portion of the assignment
* Describe your feature
-we implemented coin betting including: 
-the ability to sort the leaderboard by coins 
-all accounts start with 50 coins and can win infinite coins and can also go into debt
-doubling down works only for players with enough coins to make the bet 

* Why did you choose this feature?

Gambling is important for a healthy college experience :)
(but really it's blackjack, I'd bet it's exceedingly rare for people to play blackjack without bets)

* How did you implement it?

Coins are stored in firebase, we keep track of the contents of the betting checkbox and wager input box in MainActivity, and when we create a new game we tell the game what
wager is set for that game. Then, when a game is complete (in gameOver in MainActivity),
we update the firebase database with whatever number of coins was won/lost (in winLossUpdate).
Also, doubling down tells the game to double its wager (if the user has enough coins),
then acts as if they hit and, if they don't bust, then stands.

(10 points) Player can login and login data is stored in Firebase
(10 points) Player’s win/loss counts are displayed on startup
(10 points) The Player receives two cards face up, and the dealer receives one card face up and one card face down
(5 points) Swiping right allows the Player to hit
(5 points) Double tapping allows the Player to stand
(8 points) Cards being dealt are animated, and all cards are visible.
	I didn't see dealer cards being dealt in the app. I had to examine the code to verify that the dealer was being played properly.
(5 points) If the Player goes over 21, they automatically bust
(10 points) The dealer behaves appropriately based on the rules described above
(10 points) Once the game is complete, the winner should be declared and the Firebase database should be updated appropriately
(5 points) A new round is automatically started after each round
(5 points) Player can logout
(10 points) A leaderboard is shown in a separate tab or activity and is consistent among installations of the app
(15 points) Creative portion!

I attempted to look into the crashing error that you mentioned, however I was not able to replicate this error. Everything seemed to work fine on my end, so I'm not really sure what I can contribute to that issue.

Your app worked very well for the most part. Some minor display issues, but nothing major. I really liked your creative portion. Nice work!

Total: 108 / 110